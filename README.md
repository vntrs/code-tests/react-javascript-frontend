# Welcome to VNTRS React coding task!

Hi and welcome! We're so glad that you're interested in joining VNTRS. To get you one step closer and let you properly showcase how awesome you are at React (no awkward theoretical questions!), we prepared a short task for you to do ahead of the interview.

## The "rulebook" 🤓

- While of course it is up to you how much time you want to invest, don't feel like you have to take a week off to work on it, one afternoon is more than enough time to help us determine your current React level.
- You'll see that there is a bunch of subtasks available. That does **NOT** mean that you should solve all of them (or be able to do so in one afternoon). Instead we prefer to give you some options so you can show us your strengths, so choose wisely. Try to complete at least 2 chosen subtasks from the list, and if you feel like you have more time/energy — the more the better!
- Use your creativity and imagination. While our designers usually appreciate if you can stick to their designs, there is no limit in this test. If you feel like you have a great idea or even want to stray away from the subtasks into the unknown - go for it! (of course we will probably ask you why in the interview). 🌈

## What am I doing? 🧐

To keep the test practical, your main objective is to:

1. Use this base project to build a website Career page. The design you're building is provided [here](https://www.figma.com/file/2QDA8EL1Ep1UucKHJow1lF/React-coding-test?node-id=1:2) but you can also find it in the root of this project as `Design.png`.
2. Fetch the career data with a request to [our mock API](https://storage.googleapis.com/ext-code-test/data.json). The response is a array of objects that contains `id`, `title`, `description` and `image`. Use this data to populate the items in the grid.
3. After you've built the base, feel free to upgrade it with the subtasks below to really impress us.
4. When you're happy with the result, zip it and submit it in the Google Form you receieved, latest a day before the interview, so we can take a look!

### Subtasks: 🤩

- set up dynamic routing and link the job's to separate pages where you display the job data.
- make the page look great on all screen sizes (we all love responsive webpages)
- add some custom styling to impress us with CSS (or preprocessor) knowledge
- add an icon button to toggle the view between a grid list in the initial design and an image slider (the design of this one is up to your imagination)
- add a pop-up that opens when the user clicks on the "Help" button in the bottom right corner. The Pop-up should include a form with email and text input where a user can send us a message (you don't need to send any requests but try to set it up the way that that would be as easy to add as possible)

Good luck! 🍀
