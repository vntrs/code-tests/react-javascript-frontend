import React from "react";

const App = () => {
  return (
    <div>
        <p>
          Welcome to the React code test. Before you is a blank canvas where you can show us your skills! Good luck! 
        </p>
    </div>
  );
}

export default App;
